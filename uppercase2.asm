; Descr:  a simple program, demonstrating simple text file I/O
; (through redirection) for reading an input file to a buffer in blocks,
; forcing lowercase characters to uppercase, and writing the modified
; buffer to an output fule.
; 
; uppercase2 > output file < input_file
; 
; Build:
;     nasm -f elf64 -g -F stabs uppercase2.asm
;     ld -o uppercase2 uppercase2.o

SECTION .bss                ; section containing uninit data
    
    BUFFLEN equ 1024        ; length of buffer, like #defind boo 1024
                            ; in C language
    Buff:   resb BUFFLEN    ; text buffer itself

SECTION .data               ; section containing init data

SECTION .text               ; section containing code

global _start               ; linker need this to find the entry point

_start:
        nop                     ; this no-op keeps gdb happy...

; Read the buffer full of text from stdin:
read:
        mov eax, 3              ; specify sys_read call
        mov ebx, 0              ; specify File Descriptor 0: stdin
        mov ecx, Buff           ; pass offset of the buffer to read to
        mov edx, BUFFLEN        ; pass number of bytes to read at one pass
        int 80h                 ; call sys_read to fill the buffer
        mov esi, eax            ; copy sys_read return value for safekeeping
        cmp eax, 0             ; if eax=0, sys_read reached EOF on stdin
        je Done                 ; jump if equal (to 0, from compare)

; set up the registers for the process buffer step:
        mov ecx, esi            ; place the number of bytes read into ecx
        mov ebp, Buff           ; place address of buffer into ebp
        dec ebp                 ; adjust count to offset

; go through the buffer and convert lowercase to uppercase characters:
Scan:
        cmp byte [ebp+ecx], 61h ; test input char against lowercase 'a'
        jb Next                 ; if below 'a' in ASCII, not lowercase
        cmp byte [ebp+ecx], 7Ah ; test input char against lowercase 'z'
        ja Next                 ; if above 'z' in ASCII, not lowercase
                                ; at this point, we have a lowecase char
        sub byte [ebp+ecx], 20h ; substract 20h to give uppercase...
Next:   dec ecx                 ; decrement counter
        jnz Scan                ; if characters ramain, loop back

; write the buffer full of processed text to stdout:
Write:
        mov eax, 4              ; specify sys_write call
        mov ebx, 1              ; specify File Descriptor 1: stdout
        mov ecx, Buff           ; pass offset of the buffer
        mov edx, esi            ; pass the number of bytes of data in
                                ; the buffer
        int 80h                 ; make sys_write kernel call
        jmp read                ; loop back and load another buffer
                                ; full

; all done! let's end this anal party:
Done:   
        mov eax, 1              ; code for exit syscall
        mov ebx, 0              ; return a code of zero
        int 80h                 ; make sys_exit kernel call
