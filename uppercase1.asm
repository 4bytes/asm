section .bss
    Buff resb 1

section .data

section .text
    global _start

_start:
    nop

Read:   mov eax, 3      ; specify sys_read call
        mov ebx, 0      ; specify file desriptor 0: stdin
        mov ecx, Buff   ; pass address of the buffer to read to
        mov edx, 1      ; tell sys_read to read one char from stdin
        int 80h         ; call sys_read

        cmp eax, 0      ; look at sys_read's return value in EAX
        je Exit         ; jump if equal to 0 (0 means EOF) to Exit section
                        ; or fall through to test for lowercase
        cmp byte [Buff], 61h    ; test input char against lowercase 'a'
        jb Write        ; if below 'a' in ASCII chart, not lowercase
        cmp byte [Buff], 7Ah    ; test input char against lowercase 'z'
        ja Write        ; if above 'z' in ASCII chart, not lowercase
                        ; at this point, we have a lowercase character

        sub byte [Buff], 20h    ; substract 20h from lowercase to give uppercase...

                        ; ...and then write out the char to stdout
Write:  mov eax, 4      ; specify sys_write call
        mov ebx, 1      ; specify file desriptor 1: stdout
        mov ecx, Buff   ; pass address of the character to write
        mov edx, 1      ; pass number of chars to write
        int 80h         ; call sys_write
        jmp Read        ; then go to the beginning to get another charachter

Exit:   mov eax, 1      ; code for exit syscall
        mov ebx, 0      ; return exit code to Linux
        int 80h         ; make kernel call to exit program
