; Descr: demonstrating the conversion of binary values to hexadecimal strings.
; It acts as a very simple hex dump utility for files, though without the ASCII equivalent column.
; 
; Run:
;     hexdump1 < input_file
; 
; Build:
;     nasm -f elf64 -g -F stabs hexdump1.asm
;     ld -o hexdump1 hexdump1.o

SECTION .bss                ; section with uninit data
        BUFFLEN equ 16      ; read the file 16 bytes at a time
        Buff: resb BUFFLEN  ; text buffer itself

SECTION .data               ; section with init data
        HexStr: db " 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00", 10
        HEXLEN  equ $-HexStr

        Digits: db "0123456789ABCDEF"

SECTION .text               ; section containing code

global _start               ; linker needs this to find the entry point!

_start:
        nop                 ; no-op for gdb :)

; read a buffer full of text from stdin:
Read:
        mov eax, 3          ; sys_read call
        mov ebx, 0          ; file descriptor 0: stdin
        mov ecx, Buff       ; offset of the buffer to read to
        mov edx, BUFFLEN    ; number of bytes to read at one pass
        int 80h             ; call sys_read to fill the buffer
        mov ebp, eax        ; save number of bytes read from file for later
        cmp eax, 0          ; if eax = 0, sys_read reaches EOF on stdin
        je Done             ; Jump if Equal (to 0, from compare)

; set up the registers for the process buffer step:
        mov esi, Buff       ; place addr of file buffer into esi
        mov edi, HexStr     ; plase addr of line string into edi
        xor ecx, ecx        ; clear line string pointer to 0

; go through the buffer and convert binary values to hex digits:
Scan:
        xor eax, eax        ; clear eax to 0

; here we calculate the offset into HexStr, which is the value in ecx X 3
        mov edx, ecx        ; copy the character counter into edx
        ; shl edx, 1          ; multiply pointer by 2 using left shift 
        ; add edx, ecx        ; complete the multiplication X3
        ; The preceded 2 lines are commented because we can use lea with addr calculations inside
        lea edx, [edx*2+edx]    ; Multiply edx X 3
        ; mov edx, [edx*2+edx]    ; This stuff won't work (segmentation fault), because mov takes the value by address

; get a character from the buffer and put it in both eax and ebx;
        mov al, byte [esi+ecx]  ; put a byte from the input buffer into al
        mov ebx, eax            ; duplicate the byte in bl for second nybble

; look up low nybble character and insert it into the string:
        and al, 0Fh                 ; mask out all but the low nybble
        mov al, byte [Digits+eax]   ; look up the char equivalent of nybble
        mov byte [HexStr+edx+2], al ; write LSB char digit to line string

; look up high nybble character and insert it into the string:
        shr bl, 4                   ; shift high 4 bits of char into low 4 bits
        mov bl, byte [Digits+ebx]   ; look up char equivalent of nybble
        mov byte [HexStr+edx+1], bl ; write MSB char digit to line string

; Bump the buffer pointer to the next character and see if we're done:
        inc ecx             ; increment line string pointer
        cmp ecx, ebp        ; compare to the number of chars in the buffer
        jna Scan            ; loop back if ecx <= number of chars in buffer

; write the line of hexadecimal values to stdout:
        mov eax, 4          ; specify sys_write call
        mov ebx, 1          ; specify file descriptor 1: stdout
        mov ecx, HexStr     ; pass offset of line string
        mov edx, HEXLEN     ; pass size of the line string
        int 80h             ; make kernel call to display line string
        jmp Read            ; loop back and load file buffer again

; All done! Get out here :)
Done:
        mov eax, 1          ; code for exit syscall
        mov ebx, 0          ; return a code of zero
        int 80h             ; make kernel call
