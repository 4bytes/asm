; Descr: demonstrating the use of the XLAT instruction to alter text steams

SECTION .data               ; init data
    
    StatMsg: db "Processing...",10
    StatLen: equ $-StatMsg
    DoneMsg: db "...done!",10
    DoneLen: equ $-DoneMsg

; The following translation table translates all lowercase characters to
; uppercase. It also translates all non-printable characters to spaces,
; except for LF and HT.
    UpCase:
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,09h,0Ah,20h,20h,20h,20h,20h
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
    db 20h,21h,22h,23h,24h,25h,26h,27h,28h,29h,2Ah,2Bh,2Ch,2Dh,2Eh,2Fh
    db 30h,31h,32h,33h,34h,35h,36h,37h,38h,39h,3Ah,3Bh,3Ch,3Dh,3Eh,3Fh
    db 40h,41h,42h,43h,44h,45h,46h,47h,48h,49h,4Ah,4Bh,4Ch,4Dh,4Eh,4Fh
    db 50h,51h,52h,53h,54h,55h,56h,57h,58h,59h,5Ah,5Bh,5Ch,5Dh,5Eh,5Fh
    db 60h,41h,42h,43h,44h,45h,46h,47h,48h,49h,4Ah,4Bh,4Ch,4Dh,4Eh,4Fh
    db 50h,51h,52h,53h,54h,55h,56h,57h,58h,59h,5Ah,7Bh,7Ch,7Dh,7Eh,20h
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h 
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h 
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h 
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h 
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h 
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h 
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h 
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h 

; The following translation table is "stock" in that it translates all
; printable characters as themselves, and converts all non-printable
; chars to spaces except for LF and HT. You may modify this to translate
; anything you wnat to any character you want. 
    Custom:
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,09h,0Ah,20h,20h,20h,20h,20h
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
    db 20h,21h,22h,23h,24h,25h,26h,27h,28h,29h,2Ah,2Bh,2Ch,2Dh,2Eh,2Fh
    db 30h,31h,32h,33h,34h,35h,36h,37h,38h,39h,3Ah,3Bh,3Ch,3Dh,3Eh,3Fh
    db 40h,41h,42h,43h,44h,45h,46h,47h,48h,49h,4Ah,4Bh,4Ch,4Dh,4Eh,4Fh
    db 50h,51h,52h,53h,54h,55h,56h,57h,58h,59h,5Ah,5Bh,5Ch,5Dh,5Eh,5Fh
    db 60h,61h,62h,63h,64h,65h,66h,67h,68h,69h,6Ah,6Bh,6Ch,6Dh,6Eh,6Fh
    db 70h,71h,72h,73h,74h,75h,76h,77h,78h,79h,7Ah,7Bh,7Ch,7Dh,7Eh,20h
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h
    db 20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h,20h 

SECTION .bss            ; uninit data

    READLEN equ 1024            ; length of the buffer
    ReadBuffer: resb READLEN    ; text buffer itself

SECTION .text           ; section containing code

global _start           ; linker needs this to find the entry point

_start:
    nop                 ; for gdb

; Display the "I'm  working..." message via stderr:
    mov eax, 4          ; sys_write call
    mov ebx, 2          ; stdout
    mov ecx, StatMsg    ; offset of the message
    mov edx, StatLen    ; length of the message
    int 80h             ; do it, yeah!
    
; read a buffer full of text from stdin
read:
    mov eax, 3          ; sys_read call
    mov ebx, 0          ; stdin
    mov ecx, ReadBuffer ; offset of the buffer to read to
    mov edx, READLEN    ; number of bytes to read at one pass
    int 80h             ; :)
    mov ebp, eax        ; copy sys_read return value for safekeeping
    cmp eax, 0          ; if eax=0, sys_read reached EOF
    je done             ; Jump if Equal (to 0, from compare)

; Set up registers for the translate step:
    mov ebx, UpCase     ; place the offset of the table into ebx
    mov edx, ReadBuffer ; place the offset of the buffer into edx
    mov ecx, ebp        ; place the number of bytes in the buffer into ecx

; use the XLAT instruction to translate the data in the buffer:
; (Note: the commented out instructions do the same word as XLAT;
; un-comment them and then comment out XLAT to try it
translate:
;   xor eax, eax                ; clear high 24 bits of eax
    mov al, byte [edx+ecx]      ; load character into AL for translation
;   mov al, byte [UpCase+eax]   ; translate char in AL via table
    xlat                        ; translate char in AL via table
    mov byte [edx+ecx], al      ; put the translated char back in the buffer
    dec ecx                     ; decr char counter
    jnz translate               ; if there are more chars in the buffer, repeat

; write the buffer full of translated text to stdout:
write:
    mov eax, 4          ; sys_write call
    mov ebx, 1          ; stdout
    mov ecx, ReadBuffer ; pass offset of the buffer
    mov edx, ebp        ; pass the # of bytes of data in the buffer
    int 80h             ; oh my :)
    jmp read            ; loop back and load another buffer full

; display the "I'm done" message via stderr:
done:
    mov eax, 4          ; sys_write call
    mov ebx, 2          ; stderr
    mov ecx, DoneMsg    ; offset
    mov edx, DoneLen    ; length
    int 80h

; all done! let's end this party:
    mov eax, 1          ; exit sys call
    mov ebx, 0          ; return
    int 80h             ; boobs
